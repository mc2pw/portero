import express from 'express';
import session from 'express-session';
import helmet from 'helmet';
import connectRedis from 'connect-redis';
import config from './config';

let store;
if (config.isProduction) {
  const RedisStore = connectRedis(session);
  store = new RedisStore(config.redis);
}

const port = process.env.PORT || 3000;
const app = express();

// Based on https://expressjs.com/en/advanced/best-practice-security.html
app.use(helmet());
if (config.isProduction) app.set('trust proxy', 1);
app.use(session({ ...config.session, store }));

app.get('/', (req, res) => {
  res.send('Hello World!');
});

app.listen(port, () => console.log(`Server running on port ${port}`));
