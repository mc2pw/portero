const isProduction = process.NODE_ENV === 'production';

const defaultSessionSecret = 's3cur3';
const session = {
  secret: process.env.PORTERO_SESSION_SECRET || defaultSessionSecret,
  name: process.env.PORTERO_SESSION_NAME || 'sessionId',
  resave: false,
  saveUninitialized: false,
  cookie: {
    httpOnly: true,

  },
};

if (isProduction && session.secret === defaultSessionSecret) {
  throw new Error('PORTERO_SESSION_SECRET has to be set.');
}

if (isProduction) {
  session.cookie.secure = true;
}

const redis = {
  host: process.env.PORTERO_REDIS_HOST,
  port: process.env.PORTERO_REDIS_PORT,
  socket: process.env.PORTERO_REDIS_SOCKET,
  url: process.env.PORTERO_REDIS_URL,
};

export default {
  isProduction,
  session,
  redis,
};
