module.exports = {
  use: [
    '@neutrinojs/airbnb',
    ['@neutrinojs/node', {
      babel: {
        presets: [
          'stage-0'
        ]
      }
    }]
  ]
};
