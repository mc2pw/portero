# portero

A simple nodejs web application for handling user accounts.

Sessions are stored in Redis or Sqlite3 (for development). User records are
stored in PostgeSQL or Sqlite3 (for development).

A web application gets access to the sessions of the logged in user by running
on the same domain, and connecting to the same Redis address.

## Features

- Login form
- Logout url
- Forgot password form
- Reset password email
- Reset password form
- Change password form

## Django Admin

_portero_ comes equipped with a simple Django project _dbadmin_ to manage user
accounts through the Django Admin interface.
